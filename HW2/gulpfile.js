/** @format */

import gulp from "gulp";
const { parallel, series, src, dest, watch } = gulp;

import browserSync from "browser-sync";
const bsServer = browserSync.create();

import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);

import { deleteAsync } from "del";
import rename from "gulp-rename";

import cleanCss from "gulp-clean-css";
import groupCssMediaQueries from "gulp-group-css-media-queries";
import autoprefixer from "gulp-autoprefixer";

import concat from "gulp-concat";
import uglify from "gulp-uglify";
import imagemin from "gulp-imagemin";

function serve() {
  bsServer.init({
    server: {
      baseDir: "./",
      browser: "firefox",
    },
  });
}

function clean() {
  return deleteAsync(["dist"]);
}

function images() {
  return src("./src/img/**/*.{jpg,jpeg,png,svg,webp}")
    .pipe(imagemin())
    .pipe(dest("./dist/img/"))
    .pipe(bsServer.reload({ stream: true }));
}

function styles() {
  return src("./src/styles/style.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(groupCssMediaQueries())
    .pipe(
      autoprefixer({
        grid: true,
        overrideBrowserslist: ["last 3 versions"],
        cascade: true,
      })
    )
    .pipe(cleanCss())
    .pipe(
      rename({
        extname: ".min.css",
      })
    )
    .pipe(dest("./dist/css/"))
    .pipe(bsServer.reload({ stream: true }));
}

function scripts() {
  return src("./src/js/*.js")
    .pipe(concat("index.js"))
    .pipe(uglify())
    .pipe(
      rename({
        extname: ".min.js",
      })
    )
    .pipe(dest("./dist/js/"))
    .pipe(bsServer.reload({ stream: true }));
}

function watcher() {
  watch("./src/img/**/*.{jpg,jpeg,png,svg,webp}", images);
  watch("./src/styles/style.scss", styles);
  watch("./src/js/*.js", scripts);
  watch("./index.html").on("change", bsServer.reload);
}

export const dev = series(
  clean,
  images,
  styles,
  scripts,
  parallel(serve, watcher)
);
export const prod = series(clean, images, styles, scripts);

export default dev;
