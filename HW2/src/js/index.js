/** @format */

"use strict";

const burgerBtn = document.querySelector(".navigation__button--burger");
const closeBtn = document.querySelector(".navigation__button--close");
const list = document.querySelector(".navigation__menu");

burgerBtn.addEventListener("click", showMenu);
closeBtn.addEventListener("click", hideMenu);
function showMenu() {
  burgerBtn.style.display = "none";
  closeBtn.style.display = "block";
  list.style.display = "flex";
}
function hideMenu() {
  burgerBtn.style.display = "block";
  closeBtn.style.display = "none";
  т;
  list.style.display = "none";
}

window.addEventListener("resize", () => {
  if (window.innerWidth > 991) {
    burgerBtn.style.display = "none";
    list.style.display = "flex";
    if (closeBtn.style.display === "block") {
      closeBtn.style.display = "none";
    }
  }
  if (window.innerWidth <= 991) {
    burgerBtn.style.display = "block";
    list.style.display = "none";

    if (closeBtn.style.display === "block") {
      list.style.display = "flex";
    }
  }
});
